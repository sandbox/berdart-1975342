<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'semantic_panels_style',  // As defined in hook_schema().
  'access' => 'administer semantic_panels style settings',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/structure/semantic-panels',
    'menu item' => 'styles',
    'menu title' => 'Semantic Panels Styles',
    'menu description' => 'Administer Semantic Panels Styles.',
  ),

  // Define user interface texts.
  'title singular' => t('style'),
  'title plural' => t('styles'),
  'title singular proper' => t('Semantic Panels Style'),
  'title plural proper' => t('Semantic Panels Style'),

  'handler' => array(
    'class' => 'semantic_panels_style_ui',
    'parent' => 'ctools_export_ui',
  ),
);
