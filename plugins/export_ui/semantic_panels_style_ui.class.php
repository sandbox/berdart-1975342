<?php

/**
 * Base class for export UI.
 */
class semantic_panels_style_ui extends ctools_export_ui {

  function list_sort_options() {
    return array(
      'disabled' => t('Enabled, title'),
      'title' => t('Title'),
      'name' => t('Name'),
      'category' => t('Category'),
      'storage' => t('Storage'),
    );
  }

  function list_build_row($item, &$form_state, $operations) {
    // Set up sorting
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$item->name] = empty($item->disabled) . $item->admin_title;
        break;
      case 'title':
        $this->sorts[$item->name] = $item->admin_title;
        break;
      case 'name':
        $this->sorts[$item->name] = $item->name;
        break;
      case 'category':
        $this->sorts[$item->name] = ($item->category ? $item->category : t('Mini panels')) . $item->admin_title;
        break;
      case 'storage':
        $this->sorts[$item->name] = $item->type . $item->admin_title;
        break;
    }

    $category = $item->category ? check_plain($item->category) : '';

    $ops = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

    $this->rows[$item->name] = array(
      'data' => array(
        array('data' => check_plain($item->admin_title), 'class' => array('ctools-export-ui-title')),
        array('data' => check_plain($item->name), 'class' => array('ctools-export-ui-name')),
        array('data' => $category, 'class' => array('ctools-export-ui-category')),
        array('data' => $item->type, 'class' => array('ctools-export-ui-storage')),
        array('data' => $ops, 'class' => array('ctools-export-ui-operations')),
      ),
      'title' => !empty($item->admin_description) ? check_plain($item->admin_description) : '',
      'class' => array(!empty($item->disabled) ? 'ctools-export-ui-disabled' : 'ctools-export-ui-enabled'),
    );
  }

  function list_table_header() {
    return array(
      array('data' => t('Title'), 'class' => array('ctools-export-ui-title')),
      array('data' => t('Machine Name'), 'class' => array('ctools-export-ui-name')),
      array('data' => t('Category'), 'class' => array('ctools-export-ui-category')),
      array('data' => t('Storage'), 'class' => array('ctools-export-ui-storage')),
      array('data' => t('Operations'), 'class' => array('ctools-export-ui-operations')),
    );
  }

  function edit_form(&$form, &$form_state) {
    // Get the basic edit form
    parent::edit_form($form, $form_state);

    $form['category'] = array(
      '#type' => 'textfield',
      '#size' => 24,
      '#default_value' => isset($form_state['item']->category) ? $form_state['item']->category : '',
      '#title' => t('Category'),
      '#description' => t("The category that this mini-panel will be grouped into on the Add Content form. Only upper and lower-case alphanumeric characters are allowed. If left blank, defaults to 'Mini panels'."),
    );

    ctools_include('semantic_panels', 'semantic_panels', 'plugins/styles');
    $form += semantic_panels_pane_settings_form_custom($form_state['item']->style);
  }

  /**
   * Validate submission of the mini panel edit form.
   */
  function edit_form_basic_validate($form, &$form_state) {
    parent::edit_form_validate($form, $form_state);
    if (preg_match("/[^A-Za-z0-9 ]/", $form_state['values']['category'])) {
      form_error($form['category'], t('Categories may contain only alphanumerics or spaces.'));
    }
  }

  /**
   * Handle the submission of the edit form.
   *
   * At this point, submission is successful. Our only responsibility is
   * to copy anything out of values onto the item that we are able to edit.
   *
   * If the keys all match up to the schema, this method will not need to be
   * overridden.
   */
  function edit_form_submit(&$form, &$form_state) {
    $settings = $form_state['values']['style'];
    ctools_include('semantic_panels', 'semantic_panels', 'plugins/styles');
    semantic_panels_pane_settings_form_submit($form, $settings, $form_state);
    $form_state['values']['style'] = $settings;

    parent::edit_form_submit($form, $form_state);
  }
}